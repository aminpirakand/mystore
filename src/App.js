import React,{Component} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router,Route,Switch} from "react-router-dom";
import SignIn from "./components/authentication/SignIn";
import SignUp from "./components/authentication/SignUp";
import TopHeader from "./components/header/TopHeader";
import Header from "./components/header/Header";
import CustomNav from "./components/navbar/CustomNav";
import {Col, Row} from "react-bootstrap";
import Main from "./components/main/Main";
import MainProduct from "./components/main/product/Product";
import MainArticle from "./components/main/article/MainArticle";
import Error404 from "./components/Error404";
import About from "./components/About"
import Sidebar from "./components/sidebar/Sidebar";
import TopFooter from "./components/footer/TopFooter";
import Footer from "./components/footer/Footer";
import BottomFooter from "./components/footer/BottomFooter";
import Contact from "./components/Contact";
import Cart from "./components/cart/Cart";
import AllProduct from "./components/main/product/AllProduct";
import Product from "./components/main/product/Product";
class App extends Component {
    render() {
        return (
            <Router>
                <React.Fragment>
                    <TopHeader/>
                    <div className="App container">
                        <Header/>
                        <CustomNav/>
                        <Row className="main">
                            <Col sm={12}>
                                <Switch>
                                    <Route exact path={'/'} component={Main}/>
                                    <Route path={'/product'} component={Product}/>
                                    <Route path={'/article'} component={MainArticle}/>
                                    <Route path ={"/allproduct"} component={AllProduct}/>
                                    <Route  path="/signin" component={SignIn}/>
                                    <Route  path="/signup" component={SignUp}/>
                                    <Route path={'/error'} component={Error404}/>
                                    <Route path={'/about'} component={About} />
                                    <Route path={'/contact'} component={Contact} />
                                    <Route path={'/cart'} component={Cart}/>
                                </Switch>
                            </Col>
                        </Row>
                        <div>
                            <TopFooter/>
                            <Footer/>
                        </div>
                    </div>
                    <BottomFooter/>
                </React.Fragment>
            </Router>
        );
    }


}

export default App;
