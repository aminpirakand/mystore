import React,{useState}from "react";
import {Link} from "react-router-dom";
import productImage from "../../assets/img/productImage.jpg";
import {Row,Col} from "react-bootstrap";
import "./Cart.css"


const Cart = () =>{
    const [count, setCount] = useState(1);
    return(
        <div >
            <Row className="justify-content-between basket-product">
                <Col sm={7} className="cart">
                    <Row>
                        <div className="top">
                            <h6> نوع ارسال : </h6>
                        </div>
                    </Row>
                    <Row className="justify-content-between">
                        <Col sm={4} className="cart-image">
                            <img src={productImage} alt=""/>
                        </Col>
                        <Col sm={6} className="center">
                            <h5>اطلاعات محصول :</h5>
                            <div className="details-product">
                                <h6>رنگ :</h6>
                                <h6>گارانتی :</h6>
                                <h6>فروشگاه :</h6>
                            </div>
                            <div className="box">
                                <Row>
                                    <Col sm={4}><button className=" counter-btn" onClick={()=>setCount(count + 1)}>+</button></Col>
                                    <Col sm={4} className='number-product'><span >{count}</span></Col>
                                    <Col sm={4}>
                                        <button className="delete-btn" onClick={()=>setCount(count - 1)}>-</button>
                                    </Col>
                                </Row>

                            </div>
                        </Col>
                        <Col sm={2} className="left">
                            <h5>قیمت کالا : </h5>
                            <h6>تومان 100000</h6>
                        </Col>
                    </Row>
                </Col>
                <Col sm={4}>
                    <Row className="price">
                            <Col sm={12}>
                                <Row className="justify-content-between">
                                    <Col>قیمت کالا:</Col>
                                    <Col>25000ت</Col>
                                </Row>
                                <hr/>
                            </Col>
                            <Col sm={12}>
                                <Row className="justify-content-between">
                                    <Col>جمع:</Col>
                                    <Col>25000ت</Col>
                                </Row>
                                <hr/>
                            </Col>
                            <Col sm={12}>
                                <Row className="justify-content-between">
                                    <Col>مبلغ قابل پرداخت:</Col>
                                    <Col>27000ت</Col>
                                </Row>
                            </Col>
                    </Row>

                    <Row className="justify-content-between bottom">
                        <Col sm={6}>
                            <Row>
                                <Col sm={12} className="align-center-text">مبلغ قابل پرداخت:</Col>
                                <Col sm={12} className="align-center-text">27000ت</Col>
                            </Row>
                        </Col>
                        <Col sm={6}>
                            <Link to="/shaparak.ir" > <button className="btn btn-danger btn-block">ادامه فرایند خرید</button></Link>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    )
};
export default Cart