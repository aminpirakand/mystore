import React from "react";
import {Link,BrowserRouter as Router} from "react-router-dom";
import logo1 from "../../assets/img/logo1.jpg";
import logo2 from "../../assets/img/logo2.jpg";
import logo3 from "../../assets/img/logo3.jpg";
import "./CenterSidebar.css";
const CenterSidebar = () =>{
    return(
        <div className="sideBar">
            <div className="head-sidebar">
                <p>برند های پیشنهادی</p>
            </div>
            <div className="main-sidebar">
                <Link to='./link26'>
                    <img width={200} className="" src={logo1}/>
                </Link>
                <Link to='./link27'>
                    <img width={200} className=" " src={logo2}/>
                </Link>
                <Link to='./link28'>
                    <img width={200} className="" src={logo3}/>
                </Link>
            </div>
        </div>
    )
};
export default CenterSidebar