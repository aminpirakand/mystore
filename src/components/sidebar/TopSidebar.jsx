import React from "react";
import sidearticle from '../../assets/img/sidearticle.jpg';
import Media from 'react-bootstrap/Media';
import "./TopSidebar.css";
import {Link,BrowserRouter as Router} from 'react-router-dom';
const TopSidebar = () => {
    return(
        <div className="sideBar">
            <div className="head-sidebar">
                <p>آخرین مقالات</p>
            </div>
            <div className="main-sidebar">
                <Media>
                    <img
                        width={64}
                        height={64}
                        className="align-self-start mr-3"
                        src={sidearticle}
                    />
                    <Media.Body>
                        <Router>
                        <Link className="link-side"><p>آی او اس 11 چه امکانات فوق العاده ای دارد؟</p></Link>
                        </Router>
                    </Media.Body>

                </Media>
                <Media>
                    <img
                        width={64}
                        height={64}
                        className="align-self-start mr-3"
                        src={sidearticle}
                    />
                    <Media.Body>
                        <Router>
                        <Link className="link-side"><p>اپل واچ چه کاربردی در ورزش دارد؟</p></Link>
                        </Router>
                    </Media.Body>

                </Media>
                <Media>
                    <img
                        width={64}
                        height={64}
                        className="align-self-start mr-3"
                        src={sidearticle}
                    />
                    <Media.Body>
                        <Router>
                        <Link className="link-side"><p>آی او اس 11 چه امکانات فوق العاده ای دارد؟</p></Link>
                        </Router>
                    </Media.Body>

                </Media>
                <Media>
                    <img
                        width={64}
                        height={64}
                        className="align-self-start mr-3"
                        src={sidearticle}
                    />
                    <Media.Body>
                        <Router>
                        <Link className="link-side"><p>اپل واچ چه کاربردی در ورزش دارد؟</p></Link>
                        </Router>
                    </Media.Body>

                </Media>
                <button className="btn btn-block btn-outline-primary">
                    مشاهده تمامی مقالات
                </button>

            </div>
        </div>
    )


};
export default TopSidebar




