import React from "react";
import {Row,Col} from "react-bootstrap";
import TopSidebar from "./TopSidebar";
import CenterSidebar from "./CenterSidebar";
import BottomSidebar from "./BottomSidebar"
const Sidebar = () =>{
    return(
        <div>
            <Row>
                <Col sm={12}>
                    <TopSidebar/>
                </Col>
            </Row>
            <Row>
                <Col sm={12}>
                <CenterSidebar/>
                </Col>
            </Row>
            <Row>
                <Col sm={12}>
                    <BottomSidebar/>
                </Col>
            </Row>
        </div>
    )
};
export default Sidebar