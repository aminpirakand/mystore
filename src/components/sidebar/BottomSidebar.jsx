import React from "react";
import { Link ,BrowserRouter as Router} from "react-router-dom";
import "./BottomSidebar.css";
const BottomSidebar = () =>  {
    return (
        <div className = "sideBar" >
            <div className = "head-sidebar" >
                <p> ابزارک عمومی </p>
            </div>
            <div className = "main-sidebar last-sidebar" >
                <p>به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود.طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید</p>
                <div className = "list-link" >
                    <ul>
                        <li>
                            <Router>
                            <Link className = "link-list" to = './Link29' > پیوست یک </Link>
                            </Router>
                        </li>
                        <li >
                            <Router>
                            <Link className = "link-list" to = './Link29' > پیوست یک </Link>
                            </Router>
                        </li>
                        <li>
                            <Router>
                            <Link className = "link-list" to = './Link29' > پیوست یک </Link>
                            </Router>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
};
export default BottomSidebar