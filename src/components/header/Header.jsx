import React from "react";
import logo from "../../assets/img/logo.jpg";
import "./Header.css";
import {Link} from 'react-router-dom';

const Header = ()=>{
    return(
        <React.Fragment>
            <div className="main-shadow">
                <div className="row justify-content-between main-header">
                    <div className="col-sm-4 ">
                        <div className="mylogo">
                            <img src={logo}/>
                            <h1>فروشگاه پرسیا</h1>
                            <h4>با اطمینان خرید کنید</h4>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="left-btn-link">
                            <ul>
                                <li className="nav-item">
                                    <Link  to='/signup' className="nav-link" href="#">عضویت</Link>
                                </li>
                                <li className="nav-item">
                                    <Link  to='/signin' className="nav-link" href="#">ورود</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to='/cart' className="nav-link" href="#">سبد خرید</Link>
                                </li>
                            </ul>
                            <form action="" className="form-header">
                                <input  type="text" placeholder="جست و جو..."/>
                                <button  type="button" name="button"><i className='fas fa-search'></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
};
export default Header