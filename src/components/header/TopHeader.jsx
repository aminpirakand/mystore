import React from "react";
import {Row,Col} from "react-bootstrap";
import "./TopHeader.css";

const TopHeader = () =>{
return(
  <div className="container">
    <Row className="justify-content-between">
        <Col sm={3}>
            <div className="toparea-right">
                شماره تلفن: ۰۲۱۲۲۷۷۲۹۲۰
            </div>
        </Col>
        <Col sm={3}>
            <div className="left-top-header">
                            <span className="social-top-left telegram-icon">
                               <a href="https://t.me/" target="_blank"><i className="fab fa-telegram-plane"></i></a>
                            </span>
                <span className="social-top-left instagram-icon">
                               <a href="https://www.instagram.com/" target="_blank"><i className="fa fa-instagram"></i></a>
                            </span>
            </div>
        </Col>
    </Row>
  </div>
)
};
export default TopHeader
