import React from 'react';
import "./CustomNav.css"
import {Link} from "react-router-dom";
const MyFunction = () => {
    const x = document.getElementById("myLinks");
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
};
const CustomNav =() =>{
    return(
        <div className="nav-container">
            <nav className="navbar navbar-expand-sm bg-light justify-content-between my-nav">
                <ul className="navbar-nav justify-content-right right-ul">
                    <li className="nav-item">
                        <Link  to='/' className="nav-link nav-right-link" href="#">خانه</Link>
                        </li>
                    <li className="nav-item">
                        <Link to='/' className="nav-link nav-right-link" href="#">مقالات</Link>
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                    <Link className="nav-link nav-menu-link" to='/link9' > بازی های ایفون</Link>
                            </li>
                            <li className="nav-item">
                                    <Link className="nav-link nav-menu-link" to='/link10'>نرم افزار های اپل</Link>

                                <ul className="navbar-nav last-menu">
                                    <li className="nav-item">
                                        <Link className="nav-link nav-menu-link" to='/link9' > بازی های ایفون</Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link nav-menu-link" to='/link10'>نرم افزار های اپل</Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link nav-menu-link" to='/link11'>اپل</Link>
                                    </li>
                                </ul>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link nav-menu-link" to='/link11'>اپل</Link>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <Link to='/allproduct' className="nav-link nav-right-link" href="#">محصولات</Link>
                    </li>
                    <li>
                        <Link to='/error' className="nav-link  nav-right-link" href="#">بلاگ</Link>
                    </li>
                    <li>

                        <Link to='/about' className="nav-link nav-right-link" href="about">درباره ما</Link>

                    </li>
                    <li>
                        <Link to='/contact' className="nav-link nav-right-link" href="#">ارتباط با ما</Link>
                    </li>
                </ul>
                <ul className="navbar-nav justify-content-left left-ul">
                    <li>
                        <Link to='/link7' className="nav-link nav-left-link" href="#">آیفون 11 را ببنید</Link>
                    </li>
                </ul>
            </nav>
        </div>
    )
};
export default CustomNav