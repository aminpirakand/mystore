import React from "react";
import "./Error404.css"
import Hamyarwp from "../assets/img/404-Hamyarwp.gif"

const Error404 = () => {
    return(
        <div className="App">
            <div className="error-gif">
                <img src={Hamyarwp}/>
                <p>متاسفانه صفحه ای که دنبالش بودید در فروشگاه پرسیا وجود ندارد</p>
                <p> به صفحه ی اصلی بازگردید یا از قسمت بالای سایت جددا جستجو نمایید</p>
            </div>
        </div>
    )
};
export default Error404