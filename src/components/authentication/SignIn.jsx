import React from "react";
import "./SignIn.css"
import {Link} from "react-router-dom";

class SignIn extends React.Component{
    state = {
        email:'',
        password:''
    };
    handleChange =(e)=>{
        this.setState({
            [e.target.id]:e.target.value
        })
    };
    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state)
    };
    render() {

        return(
            <div>
                <form onSubmit={this.handleSubmit} className="sign-form" >
                    <h5 className="">ورود به حساب کاربری</h5>
                    <div className="input-field">
                        <input type="email" id="email" placeholder='ایمیل' onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <input type="password" id="password" placeholder='رمز ورود' onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <button className="btn btn-block btn-success">ورود</button>
                    </div>
                    <Link to="/"><p>رمز ورود را فراموش کرده ام؟</p></Link>

                </form>
            </div>
        )
    }
}

export default SignIn