import React from "react";
import "./SignUp.css"
class SignUp extends React.Component{
    state = {
        email:'',
        password:'',
        firstName:'',
        lastName:''
    };
    handleChange =(e)=>{
        this.setState({
            [e.target.id]:e.target.value
        })
    };
    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state)
    };
    render() {
        return(
            <div className="form-sign-up">
                <form onSubmit={this.handleSubmit} className="sign-form" >
                    <h5 className="">عضویت در سایت</h5>
                    <div className="input-field">

                        <input type="text" id="firstName" placeholder='نام' onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <input type="text" id="lastName" placeholder='نام خانوادگی' onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <input type="email" id="email" placeholder='ایمیل' onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <input type="password" id="password" placeholder='رمز ورود' onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <button className="btn btn-block btn-primary">ثبت نام</button>
                    </div>
                </form>
            </div>
        )
    }
}


export default SignUp