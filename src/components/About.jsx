import React from "react";
import {Row,Col} from "react-bootstrap";
import "./About.css";
const About = () =>{
    return(
        <div className="container about-box">
            <Row className="about">
                <Col sm={12}>
                    <h4> این سایت در جهت خدمات رسانی به شما تاسیس گردیده است </h4>
                    <p>چرسیا استور کار خود را در سال 1398 شروع کرده است و به متنی آزمایشی و بی‌معنی در </p>
                    <p>صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود.طراح گرافیک از این متن </p>
                    <p>به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید</p>
                </Col>
            </Row>
        </div>
    )
};
export default About
