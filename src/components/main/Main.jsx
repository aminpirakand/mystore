import React from "react";
import "./Main.css";
import Slider from "./Slider";
import LineProduct from "./LineProduct";
import {Row,Col} from "react-bootstrap";
import CardProduct from "./CardProduct";
import LineArticle from "./LineArticle";
import CardArticle from "./CardArticle";
import Sidebar from "../sidebar/Sidebar";

const  Main = (props) =>{
    return(
            <div>
                <Row>
                    <Col  sm={12} md={9}>
                        <Slider/>
                        <LineProduct/>
                        <Row>
                            <Col  sm={4}>
                                <CardProduct info='آیفون ایکس 128' price='2500000' id="01"/>
                            </Col>
                            <Col  sm={4}>
                                <CardProduct info='آیفون ایکس 128' price='2500000' id="01"/>

                            </Col>
                            <Col  sm={4}>
                                <CardProduct info='آیفون ایکس 128' price='2500000' id="01"/>

                            </Col>
                        </Row>
                        <Row>
                            <Col sm={4}>
                                <CardProduct info='آیفون ایکس 128' price='2500000' id="01"/>
                            </Col>
                            <Col  sm={4}>
                                <CardProduct info='آیفون ایکس 128' price='2500000' id="01"/>

                            </Col>
                            <Col  sm={4}>
                                <CardProduct info='آیفون ایکس 128' price='2500000' id="01"/>

                            </Col>
                        </Row>
                        <LineArticle/>
                        <Row>
                            <Col  sm={4}>
                                <CardArticle info='اپل واچ ؟' data='آذر 98'/>
                            </Col>
                            <Col  sm={4}>
                                <CardArticle info='اپل واچ ؟' data='آذر 98'/>
                            </Col>
                            <Col  sm={4}>
                                <CardArticle info='اپل واچ ؟' data='آذر 98'/>
                            </Col>
                        </Row>
                    </Col>
                    <Col sm={12} md={3}>
                        <Sidebar/>
                    </Col>
                </Row>
            </div>
    )
};
export default Main