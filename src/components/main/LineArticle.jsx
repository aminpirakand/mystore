import React from "react";
import "./LineArticle.css"
const LineArticle = () => {
    return(
        <div className="row justify-content-between row-card-top">
            <div className="col-md-4">
                <p>آخرین مقالات</p>
            </div>
            <div className="col-md-4 btn-left-main">
                <button className="btn btn-block btn-outline-success">مشاهده همه محصولات </button>
            </div>
        </div>
    )
};
export default LineArticle