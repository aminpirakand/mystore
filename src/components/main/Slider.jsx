import React, { useState } from 'react';
import "./Slider.css";
import Carousel from 'react-bootstrap/Carousel';
import slider from '../../assets/img/slider.jpg';
import slider2 from '../../assets/img/slider2.jpg';

const Slider = () => {
    const [index, setIndex] = useState(0);
    const [direction, setDirection] = useState(null);

    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
        setDirection(e.direction);
    };

    return (
        <div className="slider-radious">

            <Carousel activeIndex = { index }
                      direction = { direction }
                      onSelect = { handleSelect } >
                <Carousel.Item >
                    <img className = "d-block w-100 my-carousel"
                         src = { slider }
                         alt = "First slide" />
                    <Carousel.Caption>
                        <h3>آیفون ایکس</h3>
                        <p>به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item >
                    <img className = "d-block w-100 my-carousel"
                         src = { slider2 }
                         alt = "Second slide" />

                </Carousel.Item>
            </Carousel >
        </div>

    );
};

export default Slider