import React, { Component } from "react";
import Card from "react-bootstrap/Card";
import "./CardProduct.css"
import {Link} from "react-router-dom";
import articlesample from '../../assets/img/articlesample.jpg'

const CardProduct = (props) =>{
   
    return(
        
        <div>
            <Card className="all-card">
                <Card.Img  src={articlesample}/>
                <Card.Body className="title-card">
                    <h5>{props.info}</h5>
                    <div className="row justify-content-between">
                        <p id="price">{props.price}</p>
                        <Link to="/product" className="stretched-link">
                        </Link>
                        <div className="col">
                            <button className="btn btn-outline-success">
                                <i className='fas fa-shopping-cart'>
                                </i>
                            </button>
                        </div>
                    </div>
                </Card.Body>
            </Card>
        </div>
    )
};
export default CardProduct