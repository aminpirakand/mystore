import React from "react";
import "./Article.css"
import {Col, Row} from "react-bootstrap";
import imagecontentsingle from "../../../assets/img/imagecontentsingle.jpg";
import Media from "react-bootstrap/Media";
import sidearticle from '../../../assets/img/sidearticle.jpg';
const Article =()=>{
    return(
        <React.Fragment>
            <Row className="">
                <Col sm={12} className="image-article" >
                    <img src={imagecontentsingle} className="table-responsive-sm"/>
                    <p>به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود.طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید
                        به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود.طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید
                        به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود.طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید</p>
                </Col>
                <div className="article-writer ">
                    <Media>
                        <img
                            width={64}
                            height={64}
                            src={sidearticle}
                        />
                        <Media.Body>
                            <h5>این مقاله توسط وحید حنید نوشته نشده است</h5>
                            <p> گفته می‌شود.طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نمایدآی او اس 11 چه امکانات فوق العاده ای دارد؟</p>
                        </Media.Body>
                    </Media>
                </div>
            </Row>
        </React.Fragment>

    )
};
export default Article