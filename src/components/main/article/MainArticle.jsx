import React from "react";
import "./MainArticle.css";
import {Row,Col} from "react-bootstrap";
import OtherArticle from "./OtherArticl";
import Comment from "./Comment";
import CardArticle from "../CardArticle";
import Article from "./Article";
import Media from "react-bootstrap/Media";
import sidearticle from '../../../assets/img/sidearticle.jpg';
import Sidebar from "../../sidebar/Sidebar";


const MainArticle = () => {
    return(
       <div>
           <Row>
               <Col sm={9}>
                   <div className="main-product">
                       <Row>
                           <Col sm={12} className="extra-padding">
                               <Article/>
                               <Row>
                                   <div >
                                       <OtherArticle/>
                                   </div>
                                   <Row className="extra-padding">
                                       <Col sm={4}>
                                           <CardArticle info='اپل واچ ؟' data='آذر 98'/>
                                       </Col>
                                       <Col sm={4}>
                                           <CardArticle info='اپل واچ ؟' data='آذر 98'/>
                                       </Col>
                                       <Col sm={4}>
                                           <CardArticle info='اپل واچ ؟' data='آذر 98'/>
                                       </Col>
                                   </Row>
                                   <div>
                                       <Comment/>
                                   </div>
                               </Row>
                               <div className="extra-padding">
                                   <div className="row textarea ">
                                       <div className="col-sm-8">
                                <textarea placeholder="متن دیدگاه">

                                </textarea>
                                       </div>
                                       <div className="col-sm-4 left-textarea ">
                                           <form>
                                               <p>نام شما:</p>
                                               <input type="text" placeholder="نام خود را وارد کنید"/>
                                               <p>آدرس ایمیل:</p>
                                               <input type="text" placeholder="ایمیل خود را وارد کنید"/>
                                               <button className="btn  btn-success">ارسال دیدگاه</button>
                                           </form>
                                       </div>
                                   </div>
                               </div>
                               <div className="article-writer">
                                   <Media>
                                       <img
                                           width={64}
                                           height={64}
                                           src={sidearticle}
                                       />
                                       <Media.Body>
                                           <h5>این مقاله توسط وحید حنید نوشته نشده است</h5>
                                           <p> گفته می‌شود.طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نمایدآی او اس 11 چه امکانات فوق العاده ای دارد؟</p>
                                       </Media.Body>
                                   </Media>
                               </div>
                               <div className="article-writer">
                                   <Media>
                                       <img
                                           width={64}
                                           height={64}
                                           src={sidearticle}
                                       />
                                       <Media.Body>
                                           <h5>این مقاله توسط وحید حنید نوشته نشده است</h5>
                                           <p> گفته می‌شود.طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نمایدآی او اس 11 چه امکانات فوق العاده ای دارد؟</p>
                                       </Media.Body>
                                   </Media>
                               </div>
                           </Col>
                       </Row>
                   </div>
               </Col>
               <Col sm={3}>
                   <Sidebar/>
               </Col>
           </Row>
       </div>
    )
};
export default MainArticle