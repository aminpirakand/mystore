import React from "react";
import Card from "react-bootstrap/Card";
import "./CardArticle.css"
import shopping from "../../assets/img/shopping.jpeg";
import {Link} from "react-router-dom";
const CardArticle = (props) => {
    return(
        <div>
            <Card className="all-card">
                <Card.Img className="top" src={shopping}/>
                <Card.Body className="title-card">
                    <h5>{props.info}</h5>
                    <Link to="/article" className="stretched-link">
                    </Link>
                    <p id="date-article">{props.data}</p>
                </Card.Body>
            </Card>
        </div>
    )
};


export default CardArticle;