import React from "react";
import {Link} from "react-router-dom";
import "./LineProduct.css"
const LineProduct = () => {
    return(
        <div className="row justify-content-between row-card-top">
            <div className="col-md-4">
                <p>آخرین محصولات</p>
            </div>
            <div className="col-md-4 btn-left-main">
               <Link to="/allproduct"> <button className="btn btn-block btn-outline-success">مشاهده همه محصولات </button></Link>
            </div>
        </div>
    )
};
export default LineProduct