import React from "react";
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import "./MyTab.css"
const MyTab = () => {
    return(
        <div className="container my-tab">
            <Tabs defaultActiveKey="home" id="uncontrolled-tab-example" className="tabs">
                <Tab eventKey="home" title="مشخصات" className="tab">
                    <p>به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود.طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید</p>
                </Tab>
                <Tab eventKey="profile" title="نظرات" className="tab">
                    <p>به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود.طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید</p>
                </Tab>
            </Tabs>
        </div>
    )
};
export default MyTab
