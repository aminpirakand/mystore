import React,{Component} from "react";
import {Link} from "react-router-dom";
import productImage from "../../../assets/img/productImage.jpg";
import SingleProductG1 from "../../../assets/img/SingleProductG1.jpg";
import SingleProductG2 from "../../../assets/img/SingleProductG2.jpg";
import SingleProductG3 from "../../../assets/img/SingleProductG3.jpg";
import SingleProductG4 from "../../../assets/img/SingleProductG4.jpg";
import {Row,Col} from "react-bootstrap";
import "./Product.css"
import MyTab from "./MyTab";
import Sidebar from "../../sidebar/Sidebar";

class Product extends Component {
    componentDidMount(){
        window.scrollTo(0,0)
    }
    render(){
    return(
        <div>
            <Row>
                <Col sm={9}>
                    <div className="main-product">
                        <div className="row">
                            <div className="col-sm-6 my-product ">
                                <p>
                                    نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد. لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی استفاده قرار گیرد.
                                </p>
                                <hr/>
                                <p className="text-success">
                                    موجود در انبار
                                </p>
                            </div>
                            <div className="col-sm-6 img-product">
                                <img src={productImage} width="310px" height="420px" className="table-responsive-sm"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-6 my-box">
                                <div className="counter-box">
                                    <p>1</p>
                                </div>
                                <div className="btn-box">
                                    <Link to="/cart"><button className="btn btn-success">افزودن به سبد</button></Link>
                                </div>
                            </div>
                            <div className="col-sm-6 image-left-prod">
                                <div className="row image-prod">
                                    <div className="col-sm-3 ">
                                        <img src={SingleProductG1} alt="" />
                                    </div>
                                    <div className="col-sm-3">
                                        <img src={SingleProductG2} alt=""/>
                                    </div>
                                    <div className="col-sm-3">
                                        <img src={SingleProductG3} alt=""/>
                                    </div>
                                    <div className="col-sm-3">
                                        <img src={SingleProductG4} alt=""/>
                                    </div>
                                </div>
                            </div>
                            <MyTab/>
                        </div>
                    </div>
                </Col>
                <Col sm={3}>
                    <Sidebar/>
                </Col>
            </Row>
        </div>

    )
}};


export default Product