import React from "react";
import "./BottomFooter.css"
import nemad1 from "../../assets/img/nemad1.png";
import nemad2 from "../../assets/img/nemad2.png";

const BottomFooter = () =>{
    return (
        <div className="container">
            <div className="row bottom-text-footer">
                <div className="col-sm-6 text-secondary">
                    <h6>کلیه حقوق این سایت توسط دالتون ها محفوظ میباشد</h6>
                </div>
                <div className="col-sm-6">
                    <img src={nemad1}/>
                    <img src={nemad2}/>
                </div>

            </div>
        </div>
    )
};


export default BottomFooter