import React from "react";
import "./LeftFooter.css"
import {Row,Col} from "react-bootstrap";
const LeftFooter =()=>{
    return(
        <div className="left-footer">
            <h5>سایر مقالات</h5>
            <p>به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود.طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید</p>
            <form className="form-footer">
                <button>ثبت نام</button>
                <input type="email" placeholder="vahidhanid@yahoo.com"/>
            </form>
            <div className="social-footer">
                <h5>دانلود اپلیکیشن پرسیا استور</h5>
                <div className="row justify-content-between store-btn">
                    <div className="col-sm-5">
                        <button className="btn btn-block btn-secondary">IOS From AppStore</button>
                    </div>
                    <div className="col-sm-7">
                        <button className="btn btn-block btn-success">Android From GooglePlay</button>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default LeftFooter