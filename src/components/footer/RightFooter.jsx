import React from "react";
import {Link,BrowserRouter as Router} from "react-router-dom";
import "./RightFooter.css"

const RightFooter =()=>{
    return(
        <div className="footer">
            <h5 >دسته بندی محصولات</h5>
            <div className = "Link-footer" >
                <ul>
                    <li>
                        <Router>
                        <Link className = "link-list" to = './Link30' > گزینه یک </Link>
                        </Router>
                    </li>
                    <li >
                        <Router>
                        <Link className = "link-list" to = './Link31' > گزینه دو </Link>
                        </Router>
                    </li>
                    <li>
                        <Router>
                        <Link className = "link-list" to = './Link32' > گزینه سه </Link>
                        </Router>
                    </li>
                </ul>
            </div>
        </div>
    )
};


export default RightFooter