import React from "react";
import "./CenterFooter.css";
import {Link,BrowserRouter as Router} from "react-router-dom";

const CenterFooter =()=>{
    return(
        <div className="footer">
            <h5>دسته بندی مقالات</h5>
            <div className = "Link-footer" >
                <ul>
                    <li>
                        <Router>
                        <Link className = "link-list" to = './Link33' > گزینه یک </Link>
                        </Router>
                        </li>
                    <li >
                        <Router>
                        <Link className = "link-list" to = './Link34' > گزینه دو </Link>
                        </Router>
                        </li>
                    <li>
                        <Router>
                        <Link className = "link-list" to = './Link35' > گزینه سه </Link>
                        </Router>
                        </li>
                </ul>
            </div>
        </div>
    )
};


export default CenterFooter