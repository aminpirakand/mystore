import React from "react";
import "./TopFooter.css";
const TopFooter = () =>{
    return(
        <div className="top-footer">
            <div className="row justify-content-between top-footer-row">
                <div className="col-sm-6">
                    <p>برای استفاده از مشاوره های رایگان پرسیا استور میتوانید با ما تماس بگیرید </p>
                </div>

                <div className="col-sm-3">
                    <p>09019920042</p>
                </div>
            </div>

        </div>
    )
};
export default TopFooter