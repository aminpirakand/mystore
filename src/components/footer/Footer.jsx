import React from "react";
import {Row,Col} from "react-bootstrap";
import "./Footer.css"
import RightFooter from "./RightFooter";
import CenterFooter from "./CenterFooter";
import LeftFooter from "./LeftFooter";

const Footer = () =>{
    return(
        <div>
            <Row>
                <Col sm={3} className="right-footer-col">
                <RightFooter/>
                </Col>
                <Col sm={3} className="right-footer-col">
                <CenterFooter/>
                </Col>
                <Col sm={6} >
                <LeftFooter/>
                </Col>
            </Row>
        </div>
    )
};
export default Footer